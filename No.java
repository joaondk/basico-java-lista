
public class No {
	private Materia m;
	private No proximo;
	
	public No(Materia m) {
		this.m = m;
		this.proximo = null;
	}	//Constructor

	public Materia getM() {
		return m;
	}

	public void setM(Materia m) {
		this.m = m;
	}

	public No getProximo() {
		return proximo;
	}

	public void setProximo(No proximo) {
		this.proximo = proximo;
	}
	
}
