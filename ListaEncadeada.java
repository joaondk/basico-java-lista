
public class ListaEncadeada {
	private No prim;
	private No ulti;
	private int quantiNo;
	
	public ListaEncadeada() {
		this.prim = null;
		this.ulti = null;
		this.quantiNo = 0;
	}	//Constructor

	public No getPrim() {
		return prim;
	}

	public void setPrim(No prim) {
		this.prim = prim;
	}

	public No getUlti() {
		return ulti;
	}

	public void setUlti(No ulti) {
		this.ulti = ulti;
	}

	public int getQuantiNo() {
		return quantiNo;
	}

	public void setQuantiNo(int quantiNo) {
		this.quantiNo = quantiNo;
	} 
	public boolean estaVazio() {
		return (this.prim == null);
	}
		
	public void inserirPrimeiro(Materia m) {
		No novoNo = new No(m);
		if(this.estaVazio()) {
			this.ulti = novoNo;
		}
		novoNo.setProximo(this.prim);
		this.prim = novoNo;
		this.quantiNo++;
	}	//inserir primeiro
	
	public void inserirUltimo(Materia m) {
		No novoNo = new No(m);
		if(this.estaVazio()) {
			this.ulti.setProximo(novoNo);
		} else {
		novoNo.setProximo(novoNo);
		this.ulti = novoNo;
		this.quantiNo++;
		}
	}	//inserir ultimo
	
	public boolean removerNo(String materia) {
		No atual = this.prim;
		No ant = null;
		if(estaVazio()) {
			return false;
		} else {
			while((atual != null) && (!atual.getM().getMateria().equals(materia))) {
				ant = atual;
				atual = atual.getProximo();
			}
			if (atual == this.prim) {
				if (this.prim == this.ulti) {
					this.ulti = null;
				}
				this.prim = this.prim.getProximo();
			}else {
				if(atual == this.ulti) {
					this.ulti = ant;
				}
				ant.setProximo(atual.getProximo());
			}
			this.quantiNo--;
			return true;
		}
	} // remover um No
	
		public String imprimirLista() {
			String msg="";
			if (estaVazio()) {
				msg=" -> nao foi adicionado materias para esse aluno! \n";
			}else {
				No atual = this.prim;
				while(atual != null) {
					msg +=" -> "+" materia: " + atual.getM().getMateria() +" nota: " + atual.getM().getNota();
					atual = atual.getProximo();
				}
			}
			return msg;
		}	//print da lista
}
