import java.util.Scanner;

public class Principal {

	public static void main(String[] args) {
		ListaSequencial listaS = new ListaSequencial();
		boolean continuar = true;
		Scanner rgm = new Scanner(System.in);
		Scanner addmat = new Scanner(System.in);
		Scanner sc = new Scanner(System.in);
		Scanner menu = new Scanner(System.in);
		int count = 0;
		while(true) {
			System.out.println("menu");
			System.out.println("1-adicionar UM unico aluno");
			System.out.println("2-adicionar TODOS alunos");
			System.out.println("3-remover aluno por rgm");
			System.out.println("4-pesquisar aluno por rgm");
			System.out.println("5-mostrar todos os alunos");
			int inputmenu = menu.nextInt();
			switch(inputmenu) {
				case 1:
					if(!listaS.estaCheio()) {
						//for(count = 0 ;count <= 1; count++){
							
							continuar = true;		
							System.out.println("adicionando no slot "+ count);
							System.out.println("digite o RGM:");
							String inputrgm = rgm.nextLine();
						
							try {
								Integer.parseInt(inputrgm);
							} catch(NumberFormatException ignored) {
								System.out.println("isso nao e um numero valido");
								break;
							}
							
							Aluno aluno = new Aluno();
							aluno.setRgm(Integer.parseInt(inputrgm));
							listaS.inserirAluno(aluno);
							
							System.out.println("Gostaria de adicionar uma materia? digite Sim para adicionar");
							String adicionarmateria = sc.nextLine();
							
							if(adicionarmateria.equalsIgnoreCase("sim")) {
								continuar = true;
								do{//Captar matéria dada pelo usuario e add a Lista Encadeada
									Materia materia = new Materia();
									System.out.println("digite o nome da materia:");
									String mat = addmat.nextLine();
									materia.setMateria(mat);
								
									System.out.println("digite a nota da materia:");
									String not = addmat.nextLine();
									try {
										Double.parseDouble(not);
									} catch(NumberFormatException ignored) {
										System.out.println("isso nao e um numero valido");
									}
					
									materia.setNota(Double.parseDouble(not));
									aluno.materias.inserirPrimeiro(materia);
									System.out.println("Adicionar mais materia? Digite Nao para sair");
									String opc = sc.nextLine();
									if(opc.equals("Nao")) continuar = false;
								}while(continuar);
							}
							count++;
						}
					break;
				case 2:
					while(!listaS.estaCheio()) {
						//for(count = 0 ;count <= 1; count++){
							
							continuar = true;		
							System.out.println("adicionando no slot "+ count);
							System.out.println("digite o RGM:");
							String inputrgm = rgm.nextLine();
						
							try {
								Integer.parseInt(inputrgm);
							} catch(NumberFormatException ignored) {
								System.out.println("isso nao e um numero valido");
								break;
							}
							Aluno aluno = new Aluno();
							aluno.setRgm(Integer.parseInt(inputrgm));
							listaS.inserirAluno(aluno);
							System.out.println("Gostaria de adicionar uma materia? digite Sim para adicionar");
							String adicionarmateria = sc.nextLine();
							if(adicionarmateria.equalsIgnoreCase("sim")) {
								do{//Captar matéria dada pelo usuario e add a Lista Encadeada
									Materia materia = new Materia();
									System.out.println("digite o nome da materia:");
									String mat = addmat.nextLine();
									materia.setMateria(mat);
								
									System.out.println("digite a nota da materia:");
									String not = addmat.nextLine();
									try {
										Double.parseDouble(not);
									} catch(NumberFormatException ignored) {
										System.out.println("isso nao e um numero valido");
									}
					
									materia.setNota(Double.parseDouble(not));
									aluno.materias.inserirPrimeiro(materia);
									System.out.println("Adicionar mais materia? Digite Nao para sair");
									String opc = sc.nextLine();
									if(opc.equals("Nao")) continuar = false;
								}while(continuar);
							}
							
							count++;
						}
					break;
				case 3:
					System.out.println("digite o RGM:");
					String inputrgm = rgm.nextLine();
					try {
						Integer.parseInt(inputrgm);
					} catch(NumberFormatException ignored) {
						System.out.println("isso nao e um numero valido");
						break;
					}
					listaS.removerAluno(Integer.parseInt(inputrgm));
					for(int y = 0;y < listaS.tamanho;y++) {
						System.out.print(listaS.getPosicao(listaS.alunos[y])+ listaS.alunos[y].getRgm() + listaS.alunos[y].materias.imprimirLista());
					}
					break;
				case 4:
					System.out.println("digite o RGM:");
					inputrgm = rgm.nextLine();
					try {
						Integer.parseInt(inputrgm);
					} catch(NumberFormatException ignored) {
						System.out.println("isso nao e um numero valido");
						break;
					}
					int aux2 = Integer.parseInt(inputrgm);
					if(listaS.buscarBinaria(aux2) == -1){
						break;
					} else	{
						int aux = listaS.buscarBinaria(aux2);
						System.out.print(listaS.getPosicao(listaS.alunos[aux])+ " " +
								listaS.alunos[aux].getRgm() + "" +
								listaS.alunos[aux].materias.imprimirLista()); 
					}
					break;
				case 5:
					for(int y = 0;y < listaS.tamanho;y++) {
						System.out.print(listaS.getPosicao(listaS.alunos[y]) +" "+ listaS.alunos[y].getRgm() + listaS.alunos[y].materias.imprimirLista());
					}
					break;
				
			}
		}
	}
}
