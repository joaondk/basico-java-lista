
public class ListaSequencial {
	Aluno[] alunos = new Aluno[60];
	int tamanho = 0;
	
	
	public Aluno[] getAlunos() {
		return alunos;
	}

	public boolean estaVazio() {
		return (tamanho == 0);
	}
	
	public boolean estaCheio() {
		return (tamanho == alunos.length);
	}
	
	public int tamanhoLista() {
		return tamanho;
	}
	
	public Aluno buscar(int pos) {
		if ((pos >= tamanho) || (pos <0)) return null;
		
		return alunos[pos];
	}
	
	public int buscarBinaria(int rgm) {
		int inicio = 0;
		int fim = this.tamanho -1;
		int meio = 0;
		while(inicio <= fim) {
			meio = (inicio+fim) / 2;
			if(alunos[meio].getRgm() == rgm) {
				 System.out.println("Encontrei o rgm " + rgm + " em " + meio);
				 return meio;
			}
			if(alunos[meio].getRgm() < rgm) {
				inicio = meio + 1;
			} else {
				fim = meio - 1;
			}
			
		}
		if(inicio > fim) {
		      System.out.println("Não encontrou o rgm " + rgm);
		      
		}
		return -1;
	}
	
	public boolean comparar (Aluno c1, Aluno c2) {
		return (c1.getRgm() == c2.getRgm());
		
	}
	
	public int getPosicao(Aluno aluno) {
		for(int i=0;i<tamanho; i++)
			if (comparar(alunos[i], aluno))	return i;
		return -1;
	}
	
	public void deslocarDireita(int pos) {
		for (int i=tamanho;i>pos;i--)
			alunos[i]=alunos[i -1];
	}
	
	public void deslocarEsquerda(int pos) {
		for (int i=pos;i>tamanho -1;i++)
			alunos[i]=alunos[i +1];
	}
	
	public void inserirAluno(Aluno c1) {
		if (estaCheio()) return;
		if (estaVazio()) {
			alunos[0] = c1;
			tamanho++;
			return;
		}
		
		for(int i = 0;i < alunos.length;i++) {
			
			if(alunos[i] == null) {
				alunos[i] = c1; 
				this.tamanho++;
				return;
				
			}else {
				if(c1.getRgm() < alunos[i].getRgm()) {
					deslocarDireita(i);
					alunos[i] = c1;
					this.tamanho++;
					return;
				}
			}
		}
		alunos[tamanho] = c1;
		this.tamanho++;
	}
	
	public boolean removerAluno(int rgm) {
		int pos = buscarBinaria(rgm);
		if(pos < 0 || pos >=tamanho){
            return false;
		}
		
		deslocarEsquerda(pos);
        this.tamanho--;
        return true;
    }
	
}


